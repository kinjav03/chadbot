# ChadBot
ChadBot is a (currently very incomplete) Discord bot written in Rust using the [Serenity](https://github.com/serenity-rs/serenity) library. I am currently writing it for a small, private server, and to practice using Rust, but you can feel free to look at the source code.
